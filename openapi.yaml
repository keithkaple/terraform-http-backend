openapi: 3.0.0
info:
  title: Terraform HTTP backend API
  description: An implementation of the Terrafrom HTTP backend.
  version: 0.1.0
servers:
  - url: http://localhost:8080
    description: Local development
paths:
  /state/{username}:
    get:
      tags:
      - Terraform State
      summary: List names of Terraform states
      description: |
        List the names of Terraform states owned by a user.
      parameters:
      - name: username
        in: path
        description: owner of the state
        required: true
        schema:
          type: string
      security:
        - jwtBasicAuth: []
      responses:
        '200':
          description: success
          content:
            application/json:
              schema:
                type: array
                items:
                  type: string
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
  /state/{username}/{stateName}:
    get:
      tags:
      - Terraform State
      summary: Fetch Terraform state
      description: |
        Fetch the Terraform state of a Terraform state.
      parameters:
      - name: username
        in: path
        description: owner of the state
        required: true
        schema:
          type: string
      - name: stateName
        in: path
        description: name of the state, also the name of the Terraform state
        required: true
        schema:
          type: string
      security:
        - jwtBasicAuth: []
      responses:
        '200':
          description: Terraform state in JSON if sucess, empty resposne if not found
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/TerraformState"
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
    post:
      tags:
      - Terraform State
      summary: Update or create Terraform state
      description: |
        Update or create the Terraform state under a Terraform state.
      parameters:
      - name: username
        in: path
        description: owner of the state
        required: true
        schema:
          type: string
      - name: stateName
        in: path
        description: name of the state, also the name of the Terraform state
        required: true
        schema:
          type: string
      security:
        - jwtBasicAuth: []
      responses:
        '200':
          description: success
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/TerraformState"
        '401':
          $ref: '#/components/responses/Unauthorized'
        '409':
          $ref: '#/components/responses/Conflict'
        '423':
          $ref: '#/components/responses/Locked'
        '500':
          $ref: '#/components/responses/InternalServerError'
  /state/{username}/{stateName}/lock:
    post:
      tags:
      - Terraform State
      summary:  Lock Terraform state
      description: |
        Lock the Terraform state under a Terraform state.
      parameters:
      - name: username
        in: path
        description: owner of the state
        required: true
        schema:
          type: string
      - name: stateName
        in: path
        description: name of the state, also the name of the Terraform state
        required: true
        schema:
          type: string
      security:
        - jwtBasicAuth: []
      responses:
        '200':
          description: success
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '409':
          $ref: '#/components/responses/Conflict'
        '423':
          $ref: '#/components/responses/Locked'
        '500':
          $ref: '#/components/responses/InternalServerError'
  /state/{username}/{stateName}/unlock:
    post:
      tags:
      - Terraform State
      summary: Unlock Terraform state
      description: |
        Unlock the Terraform state under a Terraform state.
      parameters:
      - name: username
        in: path
        description: owner of the state
        required: true
        schema:
          type: string
      - name: stateName
        in: path
        description: name of the state, also the name of the Terraform state
        required: true
        schema:
          type: string
      security:
        - jwtBasicAuth: []
      responses:
        '200':
          description: success
        '400':
          $ref: '#/components/responses/InvalidRequest'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '409':
          $ref: '#/components/responses/Conflict'
        '500':
          $ref: '#/components/responses/InternalServerError'
  /state/{username}/{stateName}/history:
    get:
      tags:
        - Terraform State
      summary: List Terraform state histories of a Terraform state
      description: |
        Fetch the Terraform state of a Terraform state.
      parameters:
        - name: username
          in: path
          description: owner of the state
          required: true
          schema:
            type: string
        - name: stateName
          in: path
          description: name of the state, also the name of the Terraform state
          required: true
          schema:
            type: string
      security:
        - jwtBasicAuth: [ ]
      responses:
        '200':
          description: Terraform state in JSON if sucess, empty resposne if not found
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/TerraformStateHistory'
        '401':
          $ref: '#/components/responses/Unauthorized'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/InternalServerError'
components:
  schemas:
    TerraformState:
      type: object
      nullable: true
      required:
        - version
        - terraform_version
        - serial
        - lineage
        - outputs
        - resources
      properties:
        version:
          type: integer
          readOnly: true
          example: 4
        terraform_version:
          type: string
          readOnly: true
          example: "0.14.9"
        serial:
          type: integer
          readOnly: true
          example: 0
        lineage:
          type: string
          readOnly: true
          format: uuid
          example: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
        outputs:
          type: object
          readOnly: true
          example: {}
        resources:
          type: array
          readOnly: true
          items:
            $ref: '#/components/schemas/TerraformResource'
    TerraformResource:
      type: object
      properties:
        mode:
          type: string
          readOnly: true
          example: managed
        type:
          type: string
          readOnly: true
          example: local_file
        name:
          type: string
          readOnly: true
          example: foo
        provider:
          type: string
          readOnly: true
          example: provider[\"registry.terraform.io/hashicorp/local\"]
        instances:
          type: array
          readOnly: true
          items:
            $ref: '#/components/schemas/TerraformResourceInstance'
    TerraformResourceInstance:
      type: object
      required:
        - schema_version
        - attributes
        - sensitive_attributes
        - private
      properties:
        schema_version:
          type: integer
          readOnly: true
          example: 0
        attributes:
          type: object
          oneOf:
            # these are example resources, there could be others
            - $ref: '#/components/schemas/OpenStackComputeInstanceV2'
            - $ref: '#/components/schemas/OpenStackNetworkingFloatingIPV2'
            - $ref: '#/components/schemas/OpenStackComputeFloatingIPV2'
          readOnly: true
        sensitive_attributes:
          type: array
          readOnly: true
          example: []
          items:
            type: object
            description: Not sure what the type of this field is, type `object` is a placeholder.
        private:
          type: string
          readOnly: true
          format: base64 encoded json string
    OpenStackComputeInstanceV2:
      # example resource
      type: object
      description: openstack_compute_instance_v2
      required:
        - id
        # FIXME incomplete
      properties:
        access_ip_v4:
          type: string
          format: ipv4
          example: "172.16.13.83"
        access_ip_v6:
          type: string
          format: ipv6
          example: ""
        admin_pass:
          type: string
          description: type-unknown, assume it is `string`
          example: null
          nullable: true
        all_metadata:
          type: object
          description: type-unknown
          example: {}
        all_tags:
          type: array
          items:
            type: object
            description: type-unknown
          example: []
        availability_zone:
          type: string
          example: "nova"
        availability_zone_hints:
          description: type-unknown
          nullable: true
          example: null
        block_device:
          type: array
          items:
            type: object
            description: type-unknown
          example: []
        config_drive:
          description: type-unknown
          nullable: true
          example: null
        flavor_id:
          type: string
          format: uuid
          example: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
        flavor_name:
          type: string
          example: "tiny1"
        floating_ip:
          description: type-unknown
          nullable: true
          example: null
        force_delete:
          type: boolean
          example: false
        id:
          type: string
          format: uuid
          description: uuid of the VM instance
          example: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
        image_id:
          type: string
          format: uuid
          example: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
        image_name:
          type: string
          example: "ubuntu-20.04"
        key_pair:
          type: string
          example: "my_ssh_key_pair"
        metadata:
          description: type-unknown
          nullable: true
          example: null
        name:
          type: string
          description: name of the VM instance
          example: "useful_vm_instance"
        network:
          type: array
          items:
            type: object
            properties:
              access_network:
                type: boolean
                example: false
              fixed_ip_v4:
                type: string
                format: ipv4
                example: "127.0.0.1"
              fixed_ip_v6:
                type: string
                format: ipv6
                example: ""
              floating_ip:
                type: string
                example: ""
              mac:
                type: string
                format: mac
                example: "aa:aa:aa:aa:aa:aa"
              name:
                type: string
                example: "username-api-net"
              port:
                type: string
                example: ""
              uuid:
                type: string
                format: uuid
                example: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
        network_mode:
          description: type-unknown
          nullable: true
          example: null
        personality:
          type: array
          items:
            type: object
            description: type-unknown, `object` is just a placeholder
          example: []
        power_state:
          type: string
          example: "active"
        region:
          type: string
          example: "RegionOne"
        scheduler_hints:
          type: array
          items:
            type: object
            description: type-unknown, `object` is just a placeholder
          example: []
        security_groups:
          type: array
          items:
            type: string
          example:
          - "username-api-sg"
        stop_before_destroy:
          type: boolean
          example: false
        tags:
          description: type-unknown
          nullable: true
          example: null
        timeouts:
          description: type-unknown
          nullable: true
          example: null
        user_data:
          description: type-unknown
          nullable: true
          example: null
        vendor_options:
          type: array
          example: []
          items:
            type: object
            description: type-unknown, `object` is just a placeholder
        volume:
          type: array
          example: []
          items:
            type: object
            description: type-unknown, `object` is just a placeholder
    OpenStackNetworkingFloatingIPV2:
      # example resource
      type: object
      description: openstack_networking_floatingip_v2
      required:
        - address
          # FIXME incomplete
      properties:
        address:
          type: string
          format: ip
          example: "128.196.65.150"
        # FIXME incomplete
    OpenStackComputeFloatingIPV2:
      # example resource
      type: object
      description: openstack_compute_floatingip_associate_v2
      required:
        - fixed_ip
          # FIXME incomplete
      properties:
        fixed_ip:
          type: string
          example: ""
        # FIXME incomplete
    TerraformStateHistory:
      type: object
      required:
        - created_at
        - state
      properties:
        created_at:
          type: string
          readOnly: true
          format: date-time
          example: '2020-11-15T17:32:28Z'
        state:
          type: array
          nullable: true
          readOnly: true
          items:
            $ref: '#/components/schemas/TerraformState'
    Error:
      type: object
      required:
        - error
      properties:
        error:
          type: string
          readOnly: true
          format: string
          example: "invalid token"
  responses:
    NotFound:
      description: "Not found"
      content:
        "application/json":
          schema:
            $ref: "#/components/schemas/Error"
    InvalidRequest:
      description: "Invalid request"
      content:
        "application/json":
          schema:
            $ref: "#/components/schemas/Error"
    Unauthorized:
      description: "This request is unauthorized"
      content:
        "application/json":
          schema:
            $ref: "#/components/schemas/Error"
    Locked:
      description: "Resource requested is locked"
      content:
        "application/json":
          schema:
            $ref: "#/components/schemas/Error"
    Conflict:
      description: "There is a conflict with this request"
      content:
        "application/json":
          schema:
            $ref: "#/components/schemas/Error"
    InternalServerError:
      description: "Other errors"
      content:
        "application/json":
          schema:
            $ref: "#/components/schemas/Error"
  securitySchemes:
    jwtBasicAuth:
      type: http
      scheme: basic
      description: JWT token as password. The username in the token should be the same as the owner of the state.
security:
  - jwtBasicAuth: []

