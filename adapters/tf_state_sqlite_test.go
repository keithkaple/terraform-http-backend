package adapters

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/terraform-http-backend/ports"
	"testing"
)

func TestTFStateStorageSqliteImplementPort(t *testing.T) {
	var storage ports.StateStorage
	storage = &TFStateStorageSqlite{}
	assert.NotNil(t, storage)
}

func TestTFStateStorageSqlite(t *testing.T) {
	TestTFStateStorage(t, func() TFStateStorageCommon {
		return NewTFStateStorageSqliteMem()
	})
}
