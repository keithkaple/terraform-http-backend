package adapters

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/terraform-http-backend/ports"
	"gitlab.com/cyverse/terraform-http-backend/types"
	"testing"
)

func TestTFStateStoragePostgresImplementPort(t *testing.T) {
	var storage ports.StateStorage
	storage = &TFStateStoragePostgres{}
	assert.NotNil(t, storage)
}

func TestPostgresqlConnStr(t *testing.T) {
	type args struct {
		conf types.PostgresqlConfig
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "",
			args: args{
				conf: types.PostgresqlConfig{
					Host:     "localhost",
					Port:     5432,
					DBName:   "foo",
					Username: "foobar",
					Password: "password",
				},
			},
			want: "host=localhost port=5432 user=foobar password=password dbname=foo sslmode=disable",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := PostgresqlConnStr(tt.args.conf); got != tt.want {
				t.Errorf("PostgresqlConnStr() = %v, want %v", got, tt.want)
			}
		})
	}
}
