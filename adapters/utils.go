package adapters

import (
	"database/sql"
	"gitlab.com/cyverse/terraform-http-backend/types"

	log "github.com/sirupsen/logrus"
)

func sqliteCheckIfTableExists(DB *sql.DB, tableName string) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "sqliteCheckIfTableExists",
	})
	sqlStmt := `SELECT name FROM sqlite_master WHERE type='table' AND name = ?;`
	rows, err := DB.Query(sqlStmt, tableName)
	if err != nil {
		logger.WithError(err).Fatal("Query return err")
		return false, err
	}
	defer rows.Close()
	if rows == nil {
		logger.WithError(err).Error("rows is nil")
		return false, nil
	}
	for rows.Next() {
		var name string
		err = rows.Scan(&name)
		if err != nil {
			logger.WithError(err).Error("Scan failed")
			return false, err
		}
		if name == tableName {
			return true, nil
		}
	}
	return false, nil
}

func findLastHistory(histories []types.TerraformStateHistory) *types.TerraformStateHistory {
	if histories == nil || len(histories) == 0 {
		return nil
	}
	if len(histories) == 1 {
		return &histories[0]
	}
	var mostRecent = 0
	var i int
	var hist types.TerraformStateHistory
	for i, hist = range histories[1:] {
		if hist.CreatedAt.After(histories[mostRecent].CreatedAt) {
			mostRecent = i
		}
	}
	return &histories[i+1]
}
