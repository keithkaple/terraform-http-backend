package adapters

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/terraform-http-backend/ports"
)

// HTTPServer implements ports.HTTPServer
type HTTPServer struct {
	port uint
}

// NewHTTPServer ...
func NewHTTPServer(port uint) ports.HTTPServer {
	return HTTPServer{port: port}
}

// Init ...
func (server HTTPServer) Init() error {
	return nil
}

// Start ...
func (server HTTPServer) Start(r *mux.Router) error {
	addr := fmt.Sprintf(":%d", server.port)
	log.WithField("addr", addr).Info("starting http server")

	return http.ListenAndServe(addr, r)
}
