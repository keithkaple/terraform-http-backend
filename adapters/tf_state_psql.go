package adapters

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	// postgresql driver
	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/terraform-http-backend/types"
)

// TFStateStoragePostgres is a Postgresql Terraform State Storage
type TFStateStoragePostgres struct {
	db *sql.DB
}

// NewTFStateStoragePostgres ...
func NewTFStateStoragePostgres(conf types.PostgresqlConfig) *TFStateStoragePostgres {
	db, err := OpenPsql(conf)
	if err != nil {
		log.Fatal(err)
	}
	log.Info("postgres connection established")
	storage := &TFStateStoragePostgres{
		db: db,
	}
	if err = storage.initTable(); err != nil {
		log.Fatal(err)
	}
	return storage
}

// OpenPsql ...
func OpenPsql(conf types.PostgresqlConfig) (*sql.DB, error) {
	db, err := sql.Open("postgres", PostgresqlConnStr(conf))
	if err != nil {
		return nil, err
	}
	return db, nil
}

// PostgresqlConnStr ...
func PostgresqlConnStr(conf types.PostgresqlConfig) string {
	return fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		conf.Host,
		conf.Port,
		conf.Username,
		conf.Password,
		conf.DBName,
	)
}

func (storage TFStateStoragePostgres) initTable() error {
	err := storage.createStateTable()
	if err != nil {
		return err
	}
	err = storage.createHistoryTable()
	if err != nil {
		return err
	}
	return nil
}

func (storage TFStateStoragePostgres) createStateTable() error {
	sqlStmt := `CREATE TABLE IF NOT EXISTS tf_state (
	    id SERIAL PRIMARY KEY,
		owner VARCHAR(100) NOT NULL,
		state_name VARCHAR(255) NOT NULL,
    	state TEXT NULL,
		lock BOOLEAN NOT NULL,
		session_id CHAR(36) NOT NULL,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP
	);`
	_, err := storage.db.Exec(sqlStmt)
	if err != nil {
		return err
	}
	sqlStmt = "CREATE UNIQUE INDEX IF NOT EXISTS tf_state_name_owner_index ON tf_state USING btree (state_name, owner);"
	_, err = storage.db.Exec(sqlStmt)
	if err != nil {
		return err
	}
	sqlStmt = "CREATE INDEX IF NOT EXISTS tf_state_owner_index ON tf_state USING btree (owner);"
	_, err = storage.db.Exec(sqlStmt)
	if err != nil {
		return err
	}
	log.Info("table tf_state is present")
	return nil
}

func (storage TFStateStoragePostgres) createHistoryTable() error {
	sqlStmt := `CREATE TABLE IF NOT EXISTS tf_history (
		id SERIAL PRIMARY KEY,
		owner VARCHAR(100) NOT NULL,
		state_name VARCHAR(255) NOT NULL,
		state TEXT NULL,
		created_at TIMESTAMP NOT NULL
	);`
	_, err := storage.db.Exec(sqlStmt)
	if err != nil {
		return err
	}
	sqlStmt = "CREATE INDEX IF NOT EXISTS tf_hist_name_owner_index ON tf_state USING btree (state_name, owner);"
	_, err = storage.db.Exec(sqlStmt)
	if err != nil {
		return err
	}
	log.Info("table tf_history is present")
	return nil
}

// Lookup ...
func (storage TFStateStoragePostgres) Lookup(owner, stateName string) bool {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStoragePostgres.Lookup",
		"owner":     owner,
		"stateName": stateName,
	})
	row := storage.db.QueryRow("SELECT id FROM tf_state WHERE owner = $1 AND state_name = $2", owner, stateName)
	var id uint
	err := row.Scan(&id)
	if errors.Is(err, sql.ErrNoRows) {
		return false
	} else if err != nil {
		logger.WithError(err).Error()
		return false
	}
	return true
}

// Fetch ...
func (storage TFStateStoragePostgres) Fetch(owner, stateName string) (*types.TerraformState, error) {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStoragePostgres.Fetch",
		"owner":     owner,
		"stateName": stateName,
	})
	row := storage.db.QueryRow("SELECT state FROM tf_state WHERE owner = $1 AND state_name = $2", owner, stateName)
	var tfStateStr *string
	err := row.Scan(&tfStateStr)
	if errors.Is(err, sql.ErrNoRows) {
		return nil, types.ErrTFStateNotFound
	} else if err != nil {
		logger.WithError(err).Error()
		return nil, err
	}
	if tfStateStr == nil || len(*tfStateStr) == 0 {
		return nil, nil
	}
	var tfState types.TerraformState
	err = json.Unmarshal([]byte(*tfStateStr), &tfState)
	if err != nil {
		logger.WithError(err).Errorf("fail to unmarshal state as JSON")
		return nil, err
	}
	return &tfState, nil
}

// List ...
func (storage TFStateStoragePostgres) List(owner string) ([]string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "TFStateStorageSql.List",
		"owner":    owner,
	})
	rows, err := storage.db.Query("SELECT state_name FROM tf_state WHERE owner = $1", owner)
	if err != nil {
		logger.WithError(err).Error()
		return nil, err
	}
	defer rows.Close()
	if rows == nil {
	}
	stateNames := make([]string, 0)
	for rows.Next() {
		var name string
		err = rows.Scan(&name)
		if err != nil {
			return nil, err
		}
		stateNames = append(stateNames, name)
	}
	return stateNames, nil
}

// ListHistories list histories of TF state in a stateName.
func (storage TFStateStoragePostgres) ListHistories(owner, stateName string) ([]types.TerraformStateHistory, error) {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStoragePostgres.ListHistories",
		"owner":     owner,
		"stateName": stateName,
	})
	sqlStmt := `SELECT state, created_at FROM tf_history WHERE owner = $1 AND state_name = $2`
	rows, err := storage.db.Query(sqlStmt, owner, stateName)
	if err != nil {
		logger.WithError(err).Error()
		return nil, err
	}
	if rows == nil {
		return nil, errors.New("rows is nil")
	}
	histories := make([]types.TerraformStateHistory, 0)
	for rows.Next() {
		var history types.TerraformStateHistory
		var stateStr *string
		var timeStr string
		err = rows.Scan(&stateStr, &timeStr)
		if err != nil {
			logger.WithError(err).Error("scan failed")
			return nil, err
		}
		err = history.CreatedAt.UnmarshalText([]byte(timeStr))
		if err != nil {
			logger.WithError(err).Error("fail to unmarshal time string")
			return nil, err
		}
		if stateStr == nil || len(*stateStr) == 0 {
			history.State = nil
		} else {
			err = json.Unmarshal([]byte(*stateStr), &history.State)
			if err != nil {
				logger.WithError(err).Error("fail to unmarshal state string")
				return nil, err
			}
		}
		histories = append(histories, history)
	}
	return histories, nil
}

// Create ...
func (storage TFStateStoragePostgres) Create(sessionID, owner, stateName string, state types.TerraformState, opts ...types.StateCreateOption) (*types.TerraformState, error) {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStoragePostgres.Create",
		"owner":     owner,
		"stateName": stateName,
	})
	marshal, err := json.Marshal(state)
	if err != nil {
		logger.WithError(err).Error()
		return nil, err
	}
	timeNow := storage.timeNow()
	tx, err := storage.db.Begin()
	if err != nil {
		logger.WithError(err).Error("fail to begin transaction")
		return nil, err
	}
	err = storage.transactionInsertTFState(tx, owner, stateName, string(marshal), timeNow)
	if err != nil {
		logger.WithError(err).Error()
		tx.Rollback()
		return nil, err
	}
	// clear histories
	_, err = tx.Exec(`DELETE FROM tf_history WHERE owner = $1 and state_name = $2;`, owner, stateName)
	if err != nil {
		logger.WithError(err).Error()
		tx.Rollback()
		return nil, err
	}
	err = tx.Commit()
	if err != nil {
		logger.WithError(err).Error("fail to commit transaction")
		return nil, err
	}
	fetchedState, err := storage.Fetch(owner, stateName)
	if err != nil {
		logger.WithError(err).Error("fail to fetch after create")
		return nil, err
	}
	return fetchedState, nil
}

// UpdateOrCreate ...
func (storage TFStateStoragePostgres) UpdateOrCreate(sessionID, owner, stateName string, state types.TerraformState) (created bool, err error) {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStoragePostgres.UpdateOrCreate",
		"owner":     owner,
		"stateName": stateName,
	})
	marshal, err := json.Marshal(state)
	if err != nil {
		logger.WithError(err).Error()
		return false, err
	}
	tx, err := storage.db.Begin()
	if err != nil {
		logger.WithError(err).Error("fail to begin transaction")
		tx.Rollback()
		return false, err
	}
	created, err = storage.updateOrCreate(logger, tx, sessionID, owner, stateName, string(marshal))
	if err != nil {
		return false, err
	}
	err = tx.Commit()
	if err != nil {
		logger.WithError(err).Error("fail to commit transaction")
		return false, err
	}
	fetched, err := storage.Fetch(owner, stateName)
	if err != nil {
		logger.WithError(err).Error()
		return created, err
	}
	if fetched == nil {
		logger.WithField("state", fetched).Debug()
	} else {
		logger.WithField("state", *fetched).Debug()
	}
	return created, nil
}

// return created=true when the state does not exist, and thus is inserted.
func (storage TFStateStoragePostgres) updateOrCreate(logger *log.Entry, tx *sql.Tx, sessionID, owner, stateName, state string) (created bool, err error) {
	timeNow := storage.timeNow()
	exists, modifiable, err := storage.transactionCheckStateModifiable(tx, sessionID, owner, stateName)
	if err != nil {
		logger.WithError(err).Error("fail to check if stateName is modifiable, rolling back")
		tx.Rollback()
		return false, err
	}
	if !exists {
		err = storage.transactionInsertTFState(tx, owner, stateName, state, timeNow)
		if err != nil {
			logger.WithError(err).Error("fail to insert state, rolling back")
			tx.Rollback()
			return false, err
		}
		return true, nil
	} else if modifiable {
		// only insert history if the state is updated (not created)
		err = storage.transactionInsertHistory(tx, owner, stateName, timeNow)
		if err != nil {
			logger.WithError(err).Error("fail to insert history, rolling back")
			tx.Rollback()
			return false, err
		}
		// only update if not locked, or locked by the same session
		err = storage.transactionUpdateTFState(tx, owner, stateName, state, timeNow)
		if err != nil {
			logger.WithError(err).Error("fail to update state, rolling back")
			tx.Rollback()
			return false, err
		}
	} else {
		// not modifiable
		tx.Rollback()
		logger.WithError(err).Error("not modifiable")
		return false, types.ErrTFStateLockedByOthers
	}
	return false, nil
}

// Check if a stateName exists and if it is modifiable.
// A stateName is only modifiable if it is not locked, or it is locked by the same session ID that now attempts to modify it.
// Modification means operation like update/delete/lock/unlock.
func (storage TFStateStoragePostgres) transactionCheckStateModifiable(tx *sql.Tx, sessionID, owner, stateName string) (exist, modifiable bool, err error) {
	sqlStmt := `SELECT lock, session_id FROM tf_state WHERE owner = $1 AND state_name = $2;`
	row := tx.QueryRow(sqlStmt, owner, stateName)
	var locked bool
	var fetchedSessionID string
	err = row.Scan(&locked, &fetchedSessionID)
	if errors.Is(err, sql.ErrNoRows) {
		return false, false, nil
	} else if err != nil {
		return false, false, err
	}
	if !locked {
		return true, true, nil
	} else if fetchedSessionID != "" && fetchedSessionID == sessionID {
		return true, true, nil
	} else {
		return true, false, nil
	}
}

func (storage TFStateStoragePostgres) transactionInsertTFState(tx *sql.Tx, owner, stateName, state string, timeNow time.Time) error {
	sqlStmt := `INSERT INTO tf_state (owner, state_name, state, lock, session_id, created_at, updated_at) VALUES ($1, $2, $3, false, '', $4, NULL);`
	_, err := tx.Exec(sqlStmt, owner, stateName, state, timeNow)
	if err != nil {
		return err
	}
	return nil
}

func (storage TFStateStoragePostgres) transactionUpdateTFState(tx *sql.Tx, owner, stateName, state string, timeNow time.Time) error {
	sqlStmt := `UPDATE tf_state SET state = $1, updated_at = $2 WHERE owner = $3 AND state_name = $4;`
	_, err := tx.Exec(sqlStmt, state, timeNow, owner, stateName)
	if err != nil {
		return err
	}
	return nil
}

func (storage TFStateStoragePostgres) transactionInsertHistory(tx *sql.Tx, owner, stateName string, timeNow time.Time) error {
	sqlStmt := `INSERT INTO tf_history (owner, state_name, state, created_at)
SELECT owner, state_name, state, $1 FROM tf_state WHERE owner = $2 AND state_name = $3;`
	_, err := tx.Exec(sqlStmt, timeNow, owner, stateName)
	if err != nil {
		return err
	}
	return nil
}

func (storage TFStateStoragePostgres) transactionInsertHistoryWithState(tx *sql.Tx, owner, stateName, state string, timeNow time.Time) error {
	sqlStmt := `INSERT INTO tf_history (owner, state_name, state, created_at) VALUES ($1, $2, $3, $4);`
	_, err := tx.Exec(sqlStmt, owner, stateName, state, timeNow)
	if err != nil {
		return err
	}
	return nil
}

// UpdateExisting ...
func (storage TFStateStoragePostgres) UpdateExisting(sessionID, owner, stateName string, state types.TerraformState) (*types.TerraformState, error) {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStoragePostgres.UpdateExisting",
		"owner":     owner,
		"stateName": stateName,
	})

	marshal, err := json.Marshal(state)
	if err != nil {
		logger.WithError(err).Error()
		return nil, err
	}
	tx, err := storage.db.Begin()
	if err != nil {
		logger.WithError(err).Error()
		return nil, err
	}
	err = storage.updateExisting(logger, tx, sessionID, owner, stateName, string(marshal))
	if err != nil {
		tx.Rollback()
		return nil, err
	}
	err = tx.Commit()
	if err != nil {
		logger.WithError(err).Error()
		return nil, err
	}
	return nil, nil
}

func (storage TFStateStoragePostgres) updateExisting(logger *log.Entry, tx *sql.Tx, sessionID, owner, stateName string, state string) error {
	timeNow := storage.timeNow()
	exists, modifiable, err := storage.transactionCheckStateModifiable(tx, sessionID, owner, stateName)
	if err != nil {
		logger.WithError(err).Error()
		return err
	}
	if !exists {
		logger.Error("stateName not found")
		return types.ErrTFStateNotFound
	}
	if !modifiable {
		err = types.ErrTFStateLockedByOthers
		logger.WithError(err).Error("not modifiable")
		return err
	}
	err = storage.transactionInsertHistory(tx, owner, stateName, timeNow)
	if err != nil {
		logger.WithError(err).Error("fail to insert history")
		return err
	}
	err = storage.transactionUpdateTFState(tx, owner, stateName, state, timeNow)
	if err != nil {
		logger.WithError(err).Error("fail to update state")
		return err
	}
	return nil
}

// Delete ...
func (storage TFStateStoragePostgres) Delete(sessionID, owner, stateName string) error {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStoragePostgres.Delete",
		"owner":     owner,
		"stateName": stateName,
	})
	tx, err := storage.db.Begin()
	if err != nil {
		logger.WithError(err).Error("fail to start transaction")
		return err
	}
	exists, modifiable, err := storage.transactionCheckStateModifiable(tx, sessionID, owner, stateName)
	if err != nil {
		logger.WithError(err).Error("fail to check if stateName is modifiable")
		tx.Rollback()
		return err
	}
	if !exists {
		tx.Rollback()
		return types.ErrTFStateNotFound
	}
	if !modifiable {
		// not modifiable
		err = types.ErrTFStateLockedByOthers
		logger.WithError(err).Error("not modifiable")
		tx.Rollback()
		return err
	}
	err = storage.transactionInsertHistory(tx, owner, stateName, storage.timeNow())
	if err != nil {
		logger.WithError(err).Error("fail to insert history")
		tx.Rollback()
		return err
	}
	err = storage.transactionDeleteState(tx, owner, stateName)
	if err != nil {
		logger.WithError(err).Error("fail to delete stateName")
		tx.Rollback()
		return err
	}
	err = tx.Commit()
	if err != nil {
		logger.WithError(err).Error("fail to commit transaction")
		return err
	}
	return nil
}

func (storage TFStateStoragePostgres) transactionFetchState(tx *sql.Tx, owner, stateName string) (exist bool, state string, err error) {
	sqlStmt := `SELECT state FROM tf_state WHERE owner = $1 AND state_name = $2;`
	row := tx.QueryRow(sqlStmt, owner, stateName)
	err = row.Scan(&state)
	if errors.Is(err, sql.ErrNoRows) {
		return false, "", nil
	} else if err != nil {
		return false, "", err
	}
	return true, state, nil
}

func (storage TFStateStoragePostgres) transactionDeleteState(tx *sql.Tx, owner, stateName string) error {
	_, err := tx.Exec("DELETE FROM tf_state WHERE owner = $1 AND state_name = $2", owner, stateName)
	if err != nil {
		return err
	}
	return nil
}

// DeleteHistories delete all histories of a stateName.
func (storage TFStateStoragePostgres) DeleteHistories(owner, stateName string) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStoragePostgres.DeleteHistories",
		"owner":     owner,
		"stateName": stateName,
	})
	sqlStmt := `DELETE FROM tf_history WHERE owner = $1 AND state_name = $2`
	results, err := storage.db.Exec(sqlStmt, owner, stateName)
	if err != nil {
		logger.WithError(err).Error("query failed")
		return false, err
	}
	affected, err := results.RowsAffected()
	if err != nil {
		logger.WithError(err).Error("fail to obtain rows affected")
		return false, err
	}
	if affected > 0 {
		return true, nil
	}
	return false, nil
}

// Lock ...
func (storage TFStateStoragePostgres) Lock(sessionID, owner, stateName string) error {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStoragePostgres.Lock",
		"owner":     owner,
		"stateName": stateName,
	})

	tx, err := storage.db.Begin()
	if err != nil {
		logger.WithError(err).Error("fail to begin transaction")
		return err
	}
	exists, locked, _, err := storage.transactionLookupState(tx, owner, stateName)
	if err != nil {
		logger.WithError(err).Error("unable to look up state")
		tx.Rollback()
		return err
	}
	if !exists {
		// state not exists, insert placeholder
		err = storage.transactionInsertLockedPlaceHolder(tx, sessionID, owner, stateName)
		if err != nil {
			logger.WithError(err).Error("fail to insert locked placeholder")
			tx.Rollback()
			return err
		}
		if err = tx.Commit(); err != nil {
			logger.WithError(err).Error("fail to commit")
			tx.Rollback()
			return err
		}
		tx.Rollback()
		return nil
	}
	if locked {
		logger.WithError(err).Error("state already locked")
		tx.Rollback()
		return errors.New("already locked")
	}
	err = storage.transactionLockState(tx, sessionID, owner, stateName)
	if err != nil {
		logger.WithError(err).Error("fail to lock state")
		tx.Rollback()
		return err
	}
	err = tx.Commit()
	if err != nil {
		logger.WithError(err).Error("fail to commit")
		tx.Rollback()
		return err
	}
	return nil
}

func (storage TFStateStoragePostgres) transactionLookupState(tx *sql.Tx, owner, stateName string) (exists bool, locked bool, sessionID string, err error) {
	row := tx.QueryRow("SELECT lock, session_id FROM tf_state WHERE owner = $1 AND state_name = $2", owner, stateName)
	var fetchedSessionID string
	err = row.Scan(&locked, &fetchedSessionID)
	if errors.Is(err, sql.ErrNoRows) {
		return false, false, "", nil
	} else if err != nil {
		//logger.WithError(err).Error("fail to scan row")
		return false, false, "", err
	}
	return true, locked, fetchedSessionID, nil
}

func (storage TFStateStoragePostgres) transactionInsertLockedPlaceHolder(tx *sql.Tx, sessionID, owner, stateName string) error {
	sqlStmt := `INSERT INTO tf_state (owner, state_name, state, lock, session_id, created_at, updated_at) VALUES ($1, $2, NULL, TRUE, $3, $4, NULL);`
	_, err := tx.Exec(sqlStmt, owner, stateName, sessionID, storage.timeNow())
	if err != nil {
		return err
	}
	return nil
}

func (storage TFStateStoragePostgres) transactionLockState(tx *sql.Tx, sessionID, owner, stateName string) error {
	_, err := tx.Exec("UPDATE tf_state SET lock = TRUE, session_id = $1 WHERE owner = $2 AND state_name = $3 AND lock = FALSE", sessionID, owner, stateName)
	if err != nil {
		//logger.WithError(err).Error("fail to lock state")
		tx.Rollback()
		return err
	}
	return nil
}

// Unlock ...
func (storage TFStateStoragePostgres) Unlock(sessionID, owner, stateName string) error {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStoragePostgres.Unlock",
		"owner":     owner,
		"stateName": stateName,
	})
	results, err := storage.db.Exec("UPDATE tf_state SET lock = FALSE WHERE owner = $1 AND state_name = $2 AND session_id = $3", owner, stateName, sessionID)
	if err != nil {
		logger.WithError(err).Error("fail to unlock")
		return err
	}
	affected, err := results.RowsAffected()
	if err != nil {
		logger.WithError(err).Error("fail to obtain rows affected")
		return err
	}
	if affected == 0 {
		err = fmt.Errorf("stateName didn't exist or %w", types.ErrTFStateLockedByOthers)
		logger.WithError(err).Error()
		return err
	}
	return nil
}

// Locked checks if a state is locked or not, return true if locked.
func (storage TFStateStoragePostgres) Locked(owner, stateName string) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStoragePostgres.Locked",
		"owner":     owner,
		"stateName": stateName,
	})
	row := storage.db.QueryRow("SELECT lock, session_id FROM tf_state WHERE owner = $1 AND state_name = $2", owner, stateName)
	var result struct {
		Lock      bool
		SessionID string
	}
	err := row.Scan(&result.Lock, &result.SessionID)
	if errors.Is(err, sql.ErrNoRows) {
		return false, types.ErrTFStateNotFound
	} else if err != nil {
		logger.WithError(err).Error()
		return false, err
	}
	return result.Lock, nil
}

// Close ...
func (storage TFStateStoragePostgres) Close() error {
	return storage.db.Close()
}

func (storage TFStateStoragePostgres) timeNow() time.Time {
	return time.Now().UTC()
}
