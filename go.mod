module gitlab.com/cyverse/terraform-http-backend

go 1.19

require (
	github.com/golang-jwt/jwt/v4 v4.0.0
	github.com/gorilla/mux v1.8.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lib/pq v1.10.3
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/rs/xid v1.3.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.2.2
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20201119102817-f84b799fce68 // indirect
)
