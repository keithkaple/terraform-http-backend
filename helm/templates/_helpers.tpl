{{/*
Default values for postgres backend configurations. This is used by the tf-http-postgres & tf-http-backend-postgres-config configmap.
*/}}
{{- define "terraformHTTPBackend.psqlDB" }}
{{- default "tf_backend" .Values.TF_PSQL_DB }}
{{- end }}

{{- define "terraformHTTPBackend.psqlUsername" }}
{{- default "tf_backend" .Values.TF_PSQL_User }}
{{- end }}

{{- define "terraformHTTPBackend.psqlPassword" }}
{{- default (randAlphaNum 50) .Values.TF_PSQL_PASSWORD }}
{{- end }}
