package types

import "time"

// Config is the configuration for the service
type Config struct {
	PodName        string           `envconfig:"POD_NAME"` // k8s pod name
	LogLevel       string           `envconfig:"LOG_LEVEL" default:"trace"`
	HTTPPort       uint             `envconfig:"HTTP_PORT" default:"8080"`
	URLPrefix      string           `envconfig:"URL_PREFIX"`
	JWTSecretKey   string           `envconfig:"JWT_SECRET"`
	StateStorage   StateStorageType `envconfig:"STATE_STORAGE" default:"sqlite"`
	SQLiteFilename string           `envconfig:"SQLITE_FILE" default:"tf_backend.db"`
	PostgresqlConfig
}

// StateStorageType ...
type StateStorageType string

const (
	// SqliteStateStorage uses sqlite as storage for Terraform state
	SqliteStateStorage = "sqlite"
	// PsqlStateStorage uses postgres as storage for Terraform state
	PsqlStateStorage = "postgres"
)

// TerraformState ...
type TerraformState struct {
	Version   int    `json:"version"`
	TFVersion string `json:"terraform_version"`
	Serial    int    `json:"serial"`
	// Lineage is a uuid
	Lineage   string                 `json:"lineage"`
	Output    map[string]interface{} `json:"outputs"`
	Resources []TerraformResource    `json:"resources"`
}

// TerraformResource ...
type TerraformResource map[string]interface{}

// TerraformStateHistory is an instance of history of for terraform state under a name.
type TerraformStateHistory struct {
	State *TerraformState `json:"state"`
	// timestamp of when the history is created
	CreatedAt time.Time `json:"created_at"`
}

// StateCreateOption ...
type StateCreateOption struct {
	Owner string
}

// User ...
type User struct {
	Username string   `json:"username"`
	Role     UserRole `json:"role"`
}

// UserRole role of user
type UserRole string

const (
	// AdminUserRole is admin user
	AdminUserRole UserRole = "admin"
	// NormalUserRole is no-admin
	NormalUserRole UserRole = "user"
)

// PostgresqlConfig ...
type PostgresqlConfig struct {
	Host     string `envconfig:"PSQL_HOST"`
	Port     int    `envconfig:"PSQL_PORT"`
	DBName   string `envconfig:"PSQL_DB"`
	Username string `envconfig:"PSQL_USERNAME"`
	Password string `envconfig:"PSQL_PASSWORD"`
}
