package ports

import (
	"github.com/gorilla/mux"
	"gitlab.com/cyverse/terraform-http-backend/types"
)

// HTTPServer ...
type HTTPServer interface {
	Init() error
	Start(*mux.Router) error
}

// StateStorage is storage for TerraformState
type StateStorage interface {
	Lookup(owner, stateName string) bool
	Fetch(owner, stateName string) (*types.TerraformState, error)
	List(owner string) ([]string, error)
	ListHistories(owner, stateName string) ([]types.TerraformStateHistory, error)
	Create(sessionID, owner, stateName string, state types.TerraformState, opts ...types.StateCreateOption) (*types.TerraformState, error)
	// UpdateOrCreate return true when state does not exist, and thus is created
	UpdateOrCreate(sessionID, owner, stateName string, state types.TerraformState) (created bool, err error)
	UpdateExisting(sessionID, owner, stateName string, state types.TerraformState) (*types.TerraformState, error)
	Delete(sessionID, owner, stateName string) error
	Lock(sessionID, owner, stateName string) error
	Unlock(sessionID, owner, stateName string) error
	Locked(owner, stateName string) (bool, error)
	Close() error
}
