package api

import (
	"bytes"
	"encoding/json"
	"errors"
	"gitlab.com/cyverse/terraform-http-backend/types"
	"io"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/terraform-http-backend/domain/auth"
	"gitlab.com/cyverse/terraform-http-backend/domain/tfbackend"
)

// TFStateAPI contains endpoints for Terraform state management.
// Includes the Core Terraform HTTP backend endpoints that Terraform CLI will use.
type TFStateAPI struct {
	backend    tfbackend.TerraformBackend
	authDriver auth.Driver
}

// NewTFStateAPI ...
func NewTFStateAPI(tfBackend tfbackend.TerraformBackend, authDriver auth.Driver) *TFStateAPI {
	return &TFStateAPI{backend: tfBackend, authDriver: authDriver}
}

// Router register the endpoints for TFStateAPI
func (api TFStateAPI) Router(r *mux.Router, pathPrefix string) {
	subRouter := r.PathPrefix(pathPrefix + "/state").Subrouter()
	subRouter.Use(api.authDriver.Middleware)
	subRouter.HandleFunc("/{username}/{stateName}", api.GetState).Methods(http.MethodGet)
	subRouter.HandleFunc("/{username}/{stateName}", api.UpdateState).Methods(http.MethodPost)
	subRouter.HandleFunc("/{username}/{stateName}", api.DeleteState).Methods(http.MethodDelete)
	subRouter.HandleFunc("/{username}/{stateName}/lock", api.LockState).Methods(http.MethodPost)
	subRouter.HandleFunc("/{username}/{stateName}/unlock", api.UnlockState).Methods(http.MethodPost)
	subRouter.HandleFunc("/{username}/{stateName}", api.LockState).Methods("LOCK")
	subRouter.HandleFunc("/{username}/{stateName}", api.UnlockState).Methods("UNLOCK")
	subRouter.HandleFunc("/{username}", api.ListStates).Methods(http.MethodGet)
	subRouter.HandleFunc("/{username}/{stateName}/history", api.ListStateHistories).Methods(http.MethodGet)
}

// GetState get Terraform state of the stateName.
func (api TFStateAPI) GetState(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(map[string]interface{}{
		"function": "TFStateAPI.GetState",
		"method":   r.Method,
		"url":      r.URL.String(),
	})
	owner, stateName := api.parseURL(r)
	if !api.authorizationCheck(api.getUsernameFromRequestContext(r), owner) {
		api.jsonErrorStr(logger, w, http.StatusUnauthorized, "not authorized")
		return
	}
	tfState, err := api.backend.GetState(owner, stateName)
	if err != nil {
		api.backendJSONError(logger, w, err)
		return
	}
	if tfState == nil {
		// state is empty
		api.httpOk(logger, w)
		return
	}
	data, err := json.Marshal(tfState)
	if err != nil {
		api.jsonError(logger, w, http.StatusInternalServerError, err)
		return
	}
	logger.WithField("len", len(data)).Debug("fetched state")
	logger.Debug(string(data))
	api.httpOkWithBody(logger, w, data)
}

// ListStates list all Terraform states for a user
func (api TFStateAPI) ListStates(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(map[string]interface{}{
		"function": "TFStateAPI.ListStates",
		"method":   r.Method,
		"url":      r.URL.String(),
	})
	owner := mux.Vars(r)["username"]
	if !api.authorizationCheck(api.getUsernameFromRequestContext(r), owner) {
		api.jsonErrorStr(logger, w, http.StatusUnauthorized, "not authorized")
		return
	}
	stateList, err := api.backend.ListStates(owner)
	if err != nil {
		api.backendJSONError(logger, w, err)
		return
	}
	marshal, err := json.Marshal(stateList)
	if err != nil {
		api.jsonError(logger, w, http.StatusInternalServerError, err)
		return
	}
	api.httpOkWithBody(logger, w, marshal)
}

// ListStateHistories list all state histories of a Terraform stateName
func (api TFStateAPI) ListStateHistories(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(map[string]interface{}{
		"function": "TFStateAPI.ListStateHistories",
		"method":   r.Method,
		"url":      r.URL.String(),
	})
	owner, stateName := api.parseURL(r)
	if !api.authorizationCheck(api.getUsernameFromRequestContext(r), owner) {
		api.jsonErrorStr(logger, w, http.StatusUnauthorized, "not authorized")
		return
	}
	logger.Trace(owner, stateName)
	histories, err := api.backend.ListStateHistories(owner, stateName)
	if err != nil {
		api.backendJSONError(logger, w, err)
		return
	}
	if histories == nil || len(histories) == 0 {
		api.jsonErrorStr(logger, w, http.StatusNotFound, "no history")
		return
	}
	marshal, err := json.Marshal(histories)
	if err != nil {
		api.jsonError(logger, w, http.StatusInternalServerError, err)
		return
	}
	api.httpOkWithBody(logger, w, marshal)
}

// UpdateState create or update a Terraform stateName and the state associate with it.
func (api TFStateAPI) UpdateState(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(map[string]interface{}{
		"function": "TFStateAPI.UpdateState",
		"method":   r.Method,
		"url":      r.URL.String(),
	})
	logger.WithField("query_len", len(r.URL.Query())).Debug()
	for key, value := range r.URL.Query() {
		marshal, _ := json.Marshal(map[string]interface{}{key: value})
		log.Trace(string(marshal))
	}

	owner, stateName, sessionID := api.parseURLWithSessionID(r)
	if !api.authorizationCheck(api.getUsernameFromRequestContext(r), owner) {
		api.jsonErrorStr(logger, w, http.StatusUnauthorized, "not authorized")
		return
	}

	reqBody, err := io.ReadAll(r.Body)
	if err != nil {
		logger.WithError(err).Error(http.StatusInternalServerError)
		api.jsonError(logger, w, http.StatusInternalServerError, err)
		return
	}
	created, err := api.backend.UpdateOrCreateState(sessionID, owner, stateName, reqBody)
	if err != nil {
		logger.WithError(err).Error("fail to update or create")
		api.backendJSONError(logger, w, err)
		return
	}
	if created {
		logger.Info("state created")
		// create a new one
		api.httpCreated(logger, w)
	} else {
		logger.Info("state updated")
		// update the existing state
		api.httpOk(logger, w)
	}
}

// DeleteState delete a Terraform stateName and the state associate with it.
func (api TFStateAPI) DeleteState(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(map[string]interface{}{
		"function": "TFStateAPI.DeleteState",
		"method":   r.Method,
		"url":      r.URL.String(),
	})
	owner, stateName, sessionID := api.parseURLWithSessionID(r)
	if !api.authorizationCheck(api.getUsernameFromRequestContext(r), owner) {
		api.jsonErrorStr(logger, w, http.StatusUnauthorized, "not authorized")
		return
	}

	err := api.backend.DeleteState(sessionID, owner, stateName)
	if err != nil {
		logger.WithError(err).Error("fail to delete")
		api.backendJSONError(logger, w, err)
		return
	}
	logger.Info("state deleted")
	api.httpOk(logger, w)
}

// same struct for lock and unlock
type lockStateRequest struct {
	SessionID string    `json:"ID"`
	Operation string    `json:"Operation"`
	Info      string    `json:"Info"`
	Who       string    `json:"Who"`
	Version   string    `json:"Version"`
	Created   time.Time `json:"Created"`
	Path      string    `json:"Path"`
}

// LockState lock a stateName.
func (api TFStateAPI) LockState(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(map[string]interface{}{
		"function": "TFStateAPI.LockState",
		"method":   r.Method,
		"url":      r.URL.String(),
	})
	owner, stateName := api.parseURL(r)
	if !api.authorizationCheck(api.getUsernameFromRequestContext(r), owner) {
		api.jsonErrorStr(logger, w, http.StatusUnauthorized, "not authorized")
		return
	}

	var request lockStateRequest
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		api.jsonError(logger, w, http.StatusBadRequest, err)
		return
	}

	err = api.backend.LockState(request.SessionID, owner, stateName)
	if err != nil {
		logger.WithError(err).Error("fail to lock")
		api.backendJSONError(logger, w, err)
		return
	}
	logger.Info("state locked")
	api.httpOk(logger, w)
}

// UnlockState unlocks a stateName
func (api TFStateAPI) UnlockState(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(map[string]interface{}{
		"function": "TFStateAPI.UnlockState",
		"method":   r.Method,
		"url":      r.URL.String(),
	})
	owner, stateName := api.parseURL(r)
	if !api.authorizationCheck(api.getUsernameFromRequestContext(r), owner) {
		api.jsonErrorStr(logger, w, http.StatusUnauthorized, "not authorized")
		return
	}

	var request lockStateRequest
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		api.jsonError(logger, w, http.StatusBadRequest, err)
		return
	}

	err = api.backend.UnlockState(request.SessionID, owner, stateName)
	if err != nil {
		logger.WithError(err).Error("fail to unlock")
		api.backendJSONError(logger, w, err)
		return
	}
	logger.Info("state unlocked")
	api.httpOk(logger, w)
}

// get username & stateName from URL
func (api TFStateAPI) parseURL(r *http.Request) (username string, stateName string) {
	username = mux.Vars(r)["username"]
	stateName = mux.Vars(r)["stateName"]
	return username, stateName
}

// parse session ID from URL query param
func (api TFStateAPI) parseURLWithSessionID(r *http.Request) (username, stateName, sessionID string) {
	username, stateName = api.parseURL(r)
	sessionID = r.URL.Query().Get("ID")
	return
}

func (api TFStateAPI) getUsernameFromRequestContext(r *http.Request) string {
	value := r.Context().Value(auth.RequestCtxUsername)
	username, ok := value.(string)
	if !ok {
		return ""
	}
	return username
}

// return true if user is authorized to access state of an owner
func (api TFStateAPI) authorizationCheck(username, owner string) bool {
	return owner == username
}

func (api TFStateAPI) backendErrToStatusCode(err error) int {
	if errors.Is(err, types.ErrTFStateNotFound) {
		return http.StatusNotFound
	}
	if errors.Is(err, types.ErrTFStateAlreadyLocked) {
		return http.StatusLocked
	}
	if errors.Is(err, types.ErrTFStateLockedByOthers) {
		return http.StatusConflict
	}
	return http.StatusInternalServerError
}

// write a json response with error from backend
func (api TFStateAPI) backendJSONError(logger *log.Entry, w http.ResponseWriter, err error) {
	api.jsonError(logger, w, api.backendErrToStatusCode(err), err)
}

func (api TFStateAPI) jsonError(logger *log.Entry, w http.ResponseWriter, statusCode int, err error) {
	logger.WithFields(log.Fields{"status": statusCode, "err": err.Error()}).Error("responded")
	w.Header().Set("content-type", "application/json")
	w.WriteHeader(statusCode)
	var buf bytes.Buffer
	buf.WriteString(`{"error":"`)
	buf.WriteString(err.Error())
	buf.WriteString(`"}`)
	w.Write(buf.Bytes())
}

func (api TFStateAPI) jsonErrorStr(logger *log.Entry, w http.ResponseWriter, statusCode int, errMsg string) {
	logger.WithFields(log.Fields{"status": statusCode, "err": errMsg}).Error("responded")
	w.Header().Set("content-type", "application/json")
	w.WriteHeader(statusCode)
	var buf bytes.Buffer
	buf.WriteString(`{"error":"`)
	buf.WriteString(errMsg)
	buf.WriteString(`"}`)
	w.Write(buf.Bytes())
}

func (api TFStateAPI) httpCreated(logger *log.Entry, w http.ResponseWriter) {
	logger.WithField("status", http.StatusCreated).Info("responded")
	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusCreated)
}

func (api TFStateAPI) httpOk(logger *log.Entry, w http.ResponseWriter) {
	logger.WithField("status", http.StatusOK).Info("responded")
	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
}

func (api TFStateAPI) httpOkWithBody(logger *log.Entry, w http.ResponseWriter, body []byte) {
	logger.WithField("status", http.StatusOK).Info("responded")
	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, err := w.Write(body)
	if err != nil {
		logger.WithError(err).Error("fail to write response body")
		return
	}
}
