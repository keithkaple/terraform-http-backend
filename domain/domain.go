package domain

import (
	"errors"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/terraform-http-backend/domain/api"
	"gitlab.com/cyverse/terraform-http-backend/domain/auth"
	"gitlab.com/cyverse/terraform-http-backend/domain/tfbackend"
	"gitlab.com/cyverse/terraform-http-backend/ports"
	"gitlab.com/cyverse/terraform-http-backend/types"
	"net/http"
)

// Domain is the entrypoint point object to the service
type Domain struct {
	conf         types.Config
	HTTPServer   ports.HTTPServer
	StateStorage ports.StateStorage
}

// NewService ...
func NewService(conf types.Config, HTTPServer ports.HTTPServer, stateStorage ports.StateStorage) Domain {
	return Domain{conf: conf, HTTPServer: HTTPServer, StateStorage: stateStorage}
}

// Start ...
func (domain Domain) Start() error {
	log.Info("starting service")
	if err := validateConfig(domain.conf); err != nil {
		return err
	}
	if err := domain.HTTPServer.Init(); err != nil {
		return err
	}
	err := domain.HTTPServer.Start(domain.createRouter())
	if err != nil {
		return err
	}
	log.Info("domain exiting...")
	return nil
}

// validate parts of the types.Config that is relevant to Domain.
func validateConfig(conf types.Config) error {
	if conf.JWTSecretKey == "" {
		return errors.New("jwt secret key is empty")
	}
	if len(conf.JWTSecretKey) < 32 {
		return errors.New("jwt secret key length < 32")
	}
	return nil
}

func (domain Domain) createRouter() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc(domain.conf.URLPrefix+"/", domain.handleRootRequest)
	domain.registerTFStateAPI(r)
	return r
}

func (domain Domain) registerTFStateAPI(r *mux.Router) {
	tfBackend := tfbackend.NewTFBackend(domain.StateStorage)
	tfAPI := api.NewTFStateAPI(tfBackend, auth.NewJWTHTTPBasicAuth(domain.getKey))
	tfAPI.Router(r, domain.conf.URLPrefix)
}

func (domain Domain) handleRootRequest(respWriter http.ResponseWriter, req *http.Request) {
	logger := log.WithFields(map[string]interface{}{
		"method": req.Method,
		"url":    req.URL.String(),
	})

	logger.Debug("request received")
	respWriter.WriteHeader(http.StatusOK)
}

func (domain Domain) getKey() []byte {
	return []byte(domain.conf.JWTSecretKey)
}
