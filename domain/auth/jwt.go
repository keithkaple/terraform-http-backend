package auth

import (
	"bytes"
	"context"
	"errors"
	"github.com/golang-jwt/jwt/v4"
	log "github.com/sirupsen/logrus"
	"net/http"
)

// JWTHTTPBasicAuth accepts JWT token from HTTP Basic AuthDriver ("Authorization: Basic XXX")
type JWTHTTPBasicAuth struct {
	jwtAuthBase
}

// NewJWTHTTPBasicAuth ...
func NewJWTHTTPBasicAuth(keySrc func() []byte) *JWTHTTPBasicAuth {
	auth := &JWTHTTPBasicAuth{
		jwtAuthBase: jwtAuthBase{
			keySrc:         keySrc,
			tokenExtractor: tokenFromHTTPBasicAuth,
		},
	}
	return auth
}

func tokenFromHTTPBasicAuth(r *http.Request) string {
	_, password, ok := r.BasicAuth()
	if !ok {
		return ""
	}
	return password
}

// JWTAuthHeaderToken accepts JWT token from the Authorization header
type JWTAuthHeaderToken struct {
	jwtAuthBase
}

// NewJWTAuthHeaderToken ...
func NewJWTAuthHeaderToken(keySrc func() []byte) *JWTAuthHeaderToken {
	return &JWTAuthHeaderToken{
		jwtAuthBase: jwtAuthBase{
			keySrc:         keySrc,
			tokenExtractor: tokenFromAuthHeader,
		},
	}
}

func tokenFromAuthHeader(r *http.Request) string {
	return r.Header.Get("Authorization")
}

type jwtAuthBase struct {
	keySrc         func() []byte
	tokenExtractor func(r *http.Request) string
}

// Middleware is http middleware for authentication.
func (auth jwtAuthBase) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		username, err := auth.validateToken(auth.tokenExtractor(r))
		if err != nil {
			auth.jsonError(w, http.StatusUnauthorized, err)
			log.WithFields(log.Fields{
				"package":  "auth",
				"function": "jwtAuthBase.Middleware",
				"url":      r.URL.String(),
			}).WithError(err).Info("fail to validate token, 401")
			return
		}
		newCtx := context.WithValue(r.Context(), RequestCtxUsername, username)
		next.ServeHTTP(w, r.WithContext(newCtx))
	})
}

// validateToken ...
func (auth jwtAuthBase) validateToken(token string) (string, error) {
	parsedToken, err := jwt.Parse(token, auth.keyFunc)
	if err != nil {
		log.WithError(err).Error("fail to validate token")
		return "", errors.New("fail to parse token")
	}
	if parsedToken.Method == nil {
		return "", errors.New("signing method is nil")
	}
	if parsedToken.Method.Alg() != jwt.SigningMethodHS256.Name {
		return "", errors.New("unexpected signing method")
	}
	if !parsedToken.Valid {
		return "", errors.New("invalid token")
	}
	username, err := auth.getClaimAudience(parsedToken.Claims)
	if err != nil {
		return "", err
	}
	return username, nil
}

func (auth jwtAuthBase) getClaimAudience(claims jwt.Claims) (string, error) {
	stdClaims, ok := claims.(jwt.StandardClaims)
	if ok {
		return stdClaims.Audience, nil
	}
	mapClaims, ok := claims.(jwt.MapClaims)
	if !ok {
		return "", errors.New("neither standard claims nor map claim")
	}
	userClaimsRaw, ok := mapClaims["aud"]
	if !ok {
		return "", errors.New("aud missing from claim")
	}
	userClaims, ok := userClaimsRaw.(string)
	return userClaims, nil

}

func (auth jwtAuthBase) keyFunc(token *jwt.Token) (interface{}, error) {
	return auth.keySrc(), nil
}

func (auth jwtAuthBase) jsonError(w http.ResponseWriter, statusCode int, err error) {
	w.Header().Add("Content-Type", "application/json")
	w.Header().Add("WWW-Authenticate", "Basic realm=terraform-http-backend")
	w.WriteHeader(statusCode)
	var buf bytes.Buffer
	buf.WriteString(`{"error":"`)
	buf.WriteString(err.Error())
	buf.WriteString(`"}`)
	w.Write(buf.Bytes())
}

func (auth jwtAuthBase) jsonErrorStr(w http.ResponseWriter, statusCode int, errMsg string) {
	w.Header().Add("Content-Type", "application/json")
	w.Header().Add("WWW-Authenticate", "Basic realm=terraform-http-backend")
	w.WriteHeader(statusCode)
	var buf bytes.Buffer
	buf.WriteString(`{"error":"`)
	buf.WriteString(errMsg)
	buf.WriteString(`"}`)
	w.Write(buf.Bytes())
}
