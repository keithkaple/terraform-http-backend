package main

import (
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/terraform-http-backend/adapters"
	"gitlab.com/cyverse/terraform-http-backend/domain"
	"gitlab.com/cyverse/terraform-http-backend/ports"
	"gitlab.com/cyverse/terraform-http-backend/types"
)

var globalConf types.Config

func init() {
	if err := envconfig.Process("", &globalConf); err != nil {
		log.Fatal(err)
	}
	level, err := log.ParseLevel(globalConf.LogLevel)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(level)
}

func main() {
	stateStorage := newStateStorage()

	svc := domain.NewService(globalConf, adapters.NewHTTPServer(globalConf.HTTPPort), stateStorage)
	defer svc.StateStorage.Close()

	if err := svc.Start(); err != nil {
		log.Fatal(err)
	}
}

func newStateStorage() ports.StateStorage {
	var storage ports.StateStorage
	switch globalConf.StateStorage {
	case types.SqliteStateStorage:
		log.Infof("use sqlite as storage backend, %s", globalConf.SQLiteFilename)
		storage = adapters.NewTFStateStorageSqlite(globalConf.SQLiteFilename)
	case types.PsqlStateStorage:
		log.Info("use postgres as storage backend")
		storage = adapters.NewTFStateStoragePostgres(globalConf.PostgresqlConfig)
	default:
		log.Fatalf("unrecognizable state storage, \"%s\"", globalConf.StateStorage)
	}
	return storage
}
