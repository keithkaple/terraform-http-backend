FROM golang:1.19-bullseye AS builder
WORKDIR /go/src/gitlab.com/cyverse/terraform-http-backend
COPY . .
RUN go build -o terraform-http-backend .

FROM gcr.io/distroless/base-debian11
WORKDIR /
COPY --from=builder /go/src/gitlab.com/cyverse/terraform-http-backend/terraform-http-backend /
CMD ["/terraform-http-backend"]

